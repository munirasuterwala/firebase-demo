import 'package:firebase_login/services/auth.dart';
import 'package:firebase_login/utils/color_utils.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'user_list_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final  formkey=new GlobalKey<FormState>();
  String password="";
  String email="";
  AuthService authService=new AuthService();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration:BoxDecoration(
            gradient:LinearGradient(
                colors:[
                  COLOR_BACKGROUND,
                  COLOR_BACKGROUND_PURPLE
                ]
            )
        ),
        child:Form(
            key:formkey,
          child: Column(
            mainAxisAlignment:MainAxisAlignment.start,
            children: [
              Padding(
                padding:EdgeInsets.only(top:110,bottom:110),
                child:Image.asset(
                  "assets/message_circle.png",
                ),
              ),
              Stack(
                children: [
                  Image.asset(
                    "assets/login_rectangle.png",
                  ),
                  Column(
                    crossAxisAlignment:CrossAxisAlignment.start,
                    children: [

                      Padding(
                        padding: const EdgeInsets.only(top:80,left:35,right:35,bottom:40),
                        child: Image.asset(
                          "assets/login_text.png",
                        ),
                      ),
                      Material(
                        color:Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.only(top:20,left:35,right:35),
                          child: Container(
                            height:50,
                            decoration:BoxDecoration(
                                borderRadius:BorderRadius.circular(5),
                              image:DecorationImage(
                                fit:BoxFit.cover,
                                image:AssetImage("assets/username.png")
                              )
                            ),
                            child:TextFormField(
                              onChanged:(val)
                              {
                                setState(()=>email=val);
                              },
                              keyboardType:TextInputType.emailAddress,
                              validator:(value)=>value.isEmpty||!value.contains("@")?
                              "Enter valid email":null,
                              decoration:InputDecoration(
                              border:InputBorder.none,
                              hintText:"Email Address",
                              contentPadding:EdgeInsets.all(0),
                                hintStyle:TextStyle(color:COLOR_TEXT_COLOR,),
                            ),
                            style:TextStyle(color:COLOR_TEXT_COLOR,),
                            )
                          ),
                        ),
                      ),
                      Material(
                        color:Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.only(top:20,left:35,right:35),
                          child: Container(
                              height:50,
                              decoration:BoxDecoration(
                                  borderRadius:BorderRadius.circular(5),
                                  image:DecorationImage(
                                      fit:BoxFit.cover,
                                      image:AssetImage("assets/username.png")
                                  )
                              ),
                              child:TextFormField(
                                onChanged:(val)
                                {
                                  setState(() =>password=val);
                                },
                                keyboardType:TextInputType.visiblePassword,
                                validator:(value)=>value.isEmpty||value.length<=5?
                                "Enter password more than 5+ chars":
                                null,
                                decoration:InputDecoration(
                                  border:InputBorder.none,
                                  hintText:"Password",
                                  hintStyle:TextStyle(color:COLOR_TEXT_COLOR,),
                                  contentPadding:EdgeInsets.all(0),
                                ),
                                style:TextStyle(color:COLOR_TEXT_COLOR,),)
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap:() async
                        {
                          if(formkey.currentState.validate())
                            {
                              formkey.currentState.save();
                              print("all data is validate");

                              dynamic result=await authService.signinwithEmailandPassw(
                                  email,
                                  password);
                               if(result==null)
                                 {
                                   print(result.toString());
                                   print("errr");
                                   Fluttertoast.showToast(msg:"Invalid Login Credentials");
                                 }
                               else
                                 {
                                   print("user signeddd");
                                   Fluttertoast.showToast(msg:"User Successfully Signed In");
                                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder:
                                       (BuildContext context)=>UserListScreen()));
                                 }
                            }
                          else
                            {
                              Fluttertoast.showToast(msg:"Please Enter all Details");
                            }
                        },
                        child: Material(
                          color:Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.only(top:110,left:35,right:35),
                            child: Container(
                                height:50,
                                width:MediaQuery.of(context).size.width,
                                decoration:BoxDecoration(
                                    borderRadius:BorderRadius.circular(5),
                                    image:DecorationImage(
                                        fit:BoxFit.cover,
                                        image:AssetImage("assets/button_background.png")
                                    )
                                ),
                                child:Image.asset("assets/get_started_button.png"))
                            ),
                          ),
                      ),
                    ],
                  ),

                ],

              ),

            ],
          ),
        ),
      ),
    );
  }
}
