import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserListScreen extends StatefulWidget {
  @override
  _UserListScreenState createState() => _UserListScreenState();
}

class _UserListScreenState extends State<UserListScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics:ScrollPhysics(),
      child: Column(
       // physics:NeverScrollableScrollPhysics(),
        children: [
          Stack(
            children: [
              Container(
                color:Colors.white,
                  child: Image.asset("assets/user_rectangle.png",
               )),
               Padding(
               padding: const EdgeInsets.only(top:80,left:35),
                child: Image.asset("assets/hello_text.png"),
              //// child:Text("Hello John",style:TextStyle(fontWeight:FontWeight.bold,
              ////   fontSize:22,
               //  color:Colors.black,
              //   decoration:TextDecoration.none
               //   )),
               ),

            ],
          ),
          // Stack(
          //   alignment:Alignment.centerLeft,
          //   children: [
          //     Image.asset("assets/user_rectangle.png"),
          //     Padding(
          //       padding: const EdgeInsets.only(top:40,left:20),
          //       child: Image.asset("assets/hello_text.png"),
          //     )
          //   ],
          // ),
          buildList()
        ],
      ),
    );
  }

  buildList() {
    return Material(
      child: ListView.separated(
        padding:EdgeInsets.all(0),
        physics:NeverScrollableScrollPhysics(),
        shrinkWrap:true,
          itemBuilder:(BuildContext context,int index)
          {
            return ListTile(
              contentPadding:EdgeInsets.all(8),
              leading:Padding(
                padding: const EdgeInsets.only(left:20),
                child: CircleAvatar(
                  maxRadius:25,
                  backgroundImage:AssetImage("assets/dummy_profile_image.png"),
                ),
              ),
              title:Padding(
                padding:EdgeInsets.only(left:10,right:20),
                child: Text("Alex 1",style:TextStyle(
                  fontWeight:FontWeight.w700,
                ),),
              ),
              subtitle:Padding(
                padding:EdgeInsets.only(left:10,right:20),
                child: Column(
                  crossAxisAlignment:CrossAxisAlignment.start,
                  children: [
                    Padding(
                        padding:EdgeInsets.only(top:5,bottom:5),
                        child: Text("I need a heart icon in red filled",style:TextStyle(
                          fontSize:13
                        ),)),
                    Padding(
                       padding:EdgeInsets.only(bottom:5),
                        child: Text("17/02/2021 * 3:00 PM",style:TextStyle(
                          fontSize:12,
                          color:Color(0xFFA6A6A6)
                        ),)),
                  ],
                ),
              ),
            );
          },
          separatorBuilder:(BuildContext context,int index)
          {
            return Padding(
               padding:EdgeInsets.only(left:20,right:20),
                child: Image.asset("assets/line.png"));
          },
          itemCount:10),
    );
  }
}
