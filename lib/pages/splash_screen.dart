import 'dart:async';
import 'package:flutter/material.dart';
import 'package:firebase_login/utils/color_utils.dart';
import 'package:firebase_login/pages/login_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  
  @override
  void initState() {
    Timer(Duration(seconds:3),()=>Navigator.of(context).pushReplacement(MaterialPageRoute(builder:
    (BuildContext context)=>LoginScreen())));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:BoxDecoration(
        gradient:LinearGradient(
          colors:[
            COLOR_BACKGROUND,
            COLOR_BACKGROUND_PURPLE
          ]
        )
      ),
      child:Center(
        child:Image.asset(
          "assets/chat_logo.png",
        ),
      ),
    );
  }
}
