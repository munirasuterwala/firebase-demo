import 'package:firebase_auth/firebase_auth.dart';

class AuthService
{
  final FirebaseAuth auth=FirebaseAuth.instance;

  //auth change
  Stream<User> get user{
    return auth.authStateChanges().map((User user) {
      return user;
    });
  }

  //sign in(email and password)
  Future  signinwithEmailandPassw(String email,String password) async
  {
      try
        {
           var result=await auth.signInWithEmailAndPassword(email: email, password: password);
           User user=result.user;
           return user;
        }
        catch(e)
    {
      print(e.toString());
      return null;
    }
  }

}